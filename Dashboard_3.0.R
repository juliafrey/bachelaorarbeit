##### Shiny Dashboard Heavy Metals in Soil vs. Traffic data - final version #####
### Load libraries
library(shiny)
library(shinydashboard)
library(leaflet) ## this Link helped me to install package leaflet: https://askubuntu.com/questions/1299258/configure-error-gdal-config-not-found-or-not-executable-ubuntu-20-10-r-3-6?newreg=d50b5f298d4f4df4961508d2e6c264b3
library(plotly)

### Read data 

data_hmcz <- read.csv("~/R/Workspace/Bachelorarbeit/Sources/Heavy Metal City Zen/HMCZ_Data.csv", stringsAsFactors = TRUE)
location_data <- read.csv("~/R/Workspace/Bachelorarbeit/Sources/Heavy Metal City Zen/HMCZ_Data_locations.csv", stringsAsFactors = TRUE)
traffic_data <- read.csv("~/R/Workspace/Bachelorarbeit/Sources/Verkehr&Wetter/Gesamtverkehr2015.csv", stringsAsFactors = TRUE)


### Data manipulating and splitting data

longitude_data <- location_data$Longitude

latitude_data <- location_data$Latitude

pb_data <- data_hmcz$Pb..mg.kg.1.

cd_data <- data_hmcz$Cd..mg.kg.1.

zn_data <- data_hmcz$Zn..mg.kg.1.

traffic_data$GESAMTVERKEHR.KFZ.24h.MO.SO <- as.numeric(traffic_data$GESAMTVERKEHR.KFZ.24h.MO.SO)

links <- location_data$Link


### Horticultural Austrian guideline values (hagv) of the heavy metals of interest

pb_hagv <- as.numeric("100") # mg/kg d.w.
cd_hagv <- as.numeric("1") # mg/kg d.w.
zn_hagv <- as.numeric("300") # mg/kg d.w.


### Calculating of means 
## 1) Using the function aggregate()to filter the datacolumn Pb, Cd or Zn from the dataframe data_hmcz by garden and treatment to get 
## the wanted values and then define the function FUN= to calculate the mean of the specific heavy metal.

## 2) Using the function setNames() to set names for the columns for the generated new mean dataframes: pb_mean, cd_mean, zn_mean.

pb_mean <- aggregate(data_hmcz$Pb..mg.kg.1., by = list(data_hmcz$Garden, data_hmcz$Treatment), FUN = mean) #Link helped: https://stackoverflow.com/questions/31816737/how-to-calculate-the-mean-in-a-data-frame-using-aggregate-function-in-r
pb_mean <- setNames(pb_mean, c("Gardennumber","Treatment","Mean")) #Link helped: https://www.geeksforgeeks.org/change-column-name-of-a-given-dataframe-in-r/

cd_mean <- aggregate(data_hmcz$Cd..mg.kg.1., by = list(data_hmcz$Garden, data_hmcz$Treatment), FUN = mean)
cd_mean <- setNames(cd_mean, c("Gardennumber","Treatment","Mean"))

zn_mean <- aggregate(data_hmcz$Zn..mg.kg.1., by = list(data_hmcz$Garden, data_hmcz$Treatment), FUN = mean)
zn_mean <- setNames(zn_mean, c("Gardennumber","Treatment","Mean"))


### Creating new DF
## 1) Using the function data.frame() to create a new dataframe with the following six columns.
## 2) Using the function setNames() to set names for the columns for the generated new mean dataframe DF_complete.
DF_complete <- data.frame(
  col1 = c(1:11),
  col2 = traffic_data$GARDEN.NAME,
  col3 = pb_mean$Mean,
  col4 = cd_mean$Mean,
  col5 = zn_mean$Mean,
  col6 = traffic_data$GESAMTVERKEHR.KFZ.24h.MO.SO
)

DF_complete <- setNames(DF_complete, c("Gardennumber","Gardenname","Mean.Pb", "Mean.Cd", "Mean.Zn", "Traffic.in.24h.per.week"))


## Creating final DF by removing rows 1-11 from DF_complete. Only data linked to treatment "Control" remains.
DF_final <- DF_complete[-c(1:11), ]


## Change index numbers of DF_final. 
## Because of deleting rows in the last step, this was necessary to continue.
rownames(DF_final) <- NULL


### User Interface setup. 
# Is to define the interface, with which the user can interact.
ui <- dashboardPage(
  dashboardHeader(title = "Dashboard"),
  # This is the sidebar menu with three items: Dashboard, Source Code and Climate Map Vienna. 
  dashboardSidebar(
    width = 200,
    sidebarMenu(
      menuItem("Dashboard", tabName = "dashboard", icon = icon("th")), 
      menuItem("Source code", icon = icon("file-code"), 
               href = "https://gitlab.com/juliafrey/bachelaorarbeit"),
      menuItem("Climate Map Vienna", icon = icon("sun"), 
               href="https://www.wien.gv.at/stadtentwicklung/grundlagen/stadtforschung/pdf/stadtklimaanalyse-karte.pdf")
      
    )),
  # With dashboardBody() the body of the Dashboard is defined.
  dashboardBody(tabItems(
    ## Tab content: 
    ## Map and info boxes
    tabItem(tabName = "dashboard",
            fluidRow(
              ## Info boxes: Quantity Heavy Metals and Gardens/Locations
              box(#width = 6,
                         infoBox(title = "Quantity Heavy Metals", 
                                 value = tags$p(style = "font-size: 30px;", "3") , 
                                 color = "aqua", 
                                 icon = icon("info"), 
                                 width = 6),
                         infoBox(title = "Quantity Gardens", 
                                 value = tags$p(style = "font-size: 30px;", "11") , 
                                 color = "aqua", icon = icon("tree"), 
                                 width = 6)
            )), #Link helped: https://stackoverflow.com/questions/53481492/shiny-element-position-on-a-page-display-one-box-per-row-in-the-application
            
            ## Map: Leaflet
            fluidRow(
              box(title = "Map of sampling Locations", solidHeader = TRUE,
                  leafletOutput("Map", height = 250))
            ),
            
            ## Selection for plot parameters heavy metals
            fluidRow(column(6,
              ##Plot with radiobutton selection
              box(title = "Plot: Heavy Metals per Location", solidHeader = TRUE, 
                  #column(4,
                         radioButtons(inputId = "selectHM", 
                                            label = "Select Heavy Metal:",
                                            c("Lead (Pb)", "Cadmium (Cd)", "Zinc (Zn)"), 
                                            selected = "Lead (Pb)"
                         )
                  )
                  ,
                  plotlyOutput("Plot", height= 400, width = "auto"))
              #)
            )       )
  )))


### Server Setup.
## This is the server site of the shiny app. 
server <- function(input, output){
  # This is the definition of the output from the leaflet map.
  output$Map <- renderLeaflet({
    content <- paste(sep = "<br/>",
                     paste0("<a href='", location_data$Link, 
                            "' target='_blank'>", location_data$Garten, "</a>"), 
                     # helpful Link: https://community.rstudio.com/t/display-more-than-one-url-links-in-shiny-interface/8454
                     location_data$Adresse, 
                     paste(location_data$PLZ, "Wien") 
                     # helpful Link: https://databraineo.com/data-science/r-befehl-paste/
    )
    Map=leaflet() %>%
      addTiles() %>%
      addMarkers(lat=location_data$Latitude, 
                 lng = location_data$Longitude, 
                 popup = content, 
                 label = location_data$Garten) %>%
      setView(lat = 48.210033, lng = 16.363449, zoom = 10) #Vienna set as Standard View
    
  })
  
    
  
  ##Plots
  # This the definition of the plot output. An if-else if function was used, 
  # because of three different inputs, which should generate three different outputs/plots.
  output$Plot <- renderPlotly({ 
    if (input$selectHM == "Lead (Pb)")
      p <- plot_ly(data = DF_final, 
                   x=~Gardenname, 
                   y=~Mean.Pb, 
                   type = 'scatter', 
                   mode = "markers", 
                   name = "Pb mg/kg") %>%
      add_trace(data = DF_final, 
                x=~Gardenname, 
                y=~Traffic.in.24h.per.week, 
                type = "bar", 
                yaxis = "y2", 
                name = "Traffic", 
                width = 0.4) %>%
      add_lines(showlegend = FALSE, y = pb_hagv, x = NULL, 
                line = list(color = 'rgb(216, 31, 42)')) %>%
      layout(yaxis2 = list (side = "right", title = "Traffic in 24h per Week"),
               yaxis = list(overlaying = "y2", title = "Pb mg/kg"),
               xaxis = list(dtick = 1, title = "Locations"),
              legend = list(orientation = 'v')) %>%
      add_trace(y = cd_hagv, 
                x = NULL, 
                name = c("Limit Pb [mg/kg d.w.]"),
                mode = 'lines', 
                line = list(color = 'rgb(216, 31, 42)'))
      
    
    else if (input$selectHM == "Cadmium (Cd)")
      p <- plot_ly(data = DF_final, 
                   x=~Gardenname, 
                   y=~Mean.Cd, 
                   type = 'scatter', 
                   mode = "markers", 
                   name = "Cd mg/kg") %>%
        add_trace(data = DF_final, 
                  x=~Gardenname, 
                  y=~Traffic.in.24h.per.week, 
                  type = "bar", 
                  yaxis = "y2", 
                  name = "Traffic", 
                  width = 0.4)%>% #bar width
        add_lines(showlegend = FALSE, y = cd_hagv, x = NULL,
                  line = list(color = 'rgb(216, 31, 42)')) %>%
        layout(yaxis2 = list (side = "right", title = "Traffic in 24h per Week"),
               yaxis = list(overlaying = "y2", title = "Cd mg/kg"),
               xaxis = list(dtick = 1, title = "Locations"),
               legend = list(orientation = 'v'))  %>%
        add_trace(y = cd_hagv, 
                  x = NULL, 
                  name = c("Limit Cd [mg/kg d.w.]"), 
                  mode = 'lines', 
                  line = list(color = 'rgb(216, 31, 42)'))
    
    
    else if (input$selectHM == "Zinc (Zn)")
      p <- plot_ly(data = DF_final, 
                   x=~Gardenname, 
                   y=~Mean.Zn, 
                   type = 'scatter', 
                   mode = "markers", 
                   name = "Zn mg/kg") %>%
        add_trace(data = DF_final, 
                  x=~Gardenname, 
                  y=~Traffic.in.24h.per.week, 
                  type = "bar", 
                  yaxis = "y2", 
                  name = "Traffic", 
                  width = 0.4)%>%
        add_lines(showlegend = FALSE, y = zn_hagv, x = NULL,
                  line = list(color = 'rgb(216, 31, 42)')) %>%
        layout(yaxis2 = list (side = "right", title = "Traffic in 24h per Week"),
               yaxis = list(overlaying = "y2", title = "Zn mg/kg"),
               xaxis = list(dtick = 1, title = "Locations"),
               legend = list(orientation = 'v')) %>%
        add_trace(y = zn_hagv, 
                  x = NULL, 
                  name = c("Limit Zn [mg/kg d.w.]"), 
                  mode = 'lines', 
                  line = list(color = 'rgb(216, 31, 42)'))
    
  
    
    ##Link helped: https://stackoverflow.com/questions/39380227/second-y-axis-in-a-r-plotly-graph
      ##change barwidth Link helped: https://community.plotly.com/t/bar-width-in-bar-charts-not-changing/3771
   
  })
  
  
}

## User Interface and server get connected to proceed the Shiny-Application
shinyApp(ui, server)

